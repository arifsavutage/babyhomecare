<?php
/*
membuat support featured image di theme
*/
add_theme_support( 'post-thumbnails' );

/*title tag*/
add_theme_support( 'title-tag' );

/*register menu navigasi*/
function register_babyhomecare_menus(){
	register_nav_menus(
		array(
			'top-menu'		=> __( 'Top Menu' ),
			'social-menu'	=> __( 'Social Menu' ),
			'mobile-menu'	=> __( 'Mobile Menu' )
		)
	);
}
add_action('init', 'register_babyhomecare_menus');

/*
membuat custom logo di customizing
*/
function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

/*
membuat widget
*/
function babyhomecare_widgets_init(){
	register_sidebar(array(
		'name'			=> __('Jumbotron', 'babyhomecare'),
		'id'			=> 'jumbotron',
		'description'	=> __('Menampilkan text page jumbotron depan, isikan dengan text', 'babyhomecare'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '<h2 class="text-center">',
		'after_title'	=> '</h2>' 
	));

	register_sidebar(array(
		'name'			=> __('Jumbotron Image', 'babyhomecare'),
		'id'			=> 'jumbotron-image',
		'description'	=> __('Menampilkan image di jumbotron, isikan dengan image', 'babyhomecare'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '',
		'after_title'	=> ''
	));
	
	register_sidebar(array(
		'name'			=> __('Jumbotron Link', 'babyhomecare'),
		'id'			=> 'jumbotron-link',
		'description'	=> __('Menampilkn link button "hubungi kami", isikan dengan custom html', 'babyhomecare'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '',
		'after_title'	=> ''
	));
	
	register_sidebar(array(
		'name'			=> __('Section Paket 1', 'babyhomecare'),
		'id'			=> 'paket-1',
		'description'	=> __('Menampilkan tarif paket baris pertama, maksimal hanya 3', 'babyhomecare'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '',
		'after_title'	=> ''
	));
	
	register_sidebar(array(
		'name'			=> __('Section Paket 2', 'babyhomecare'),
		'id'			=> 'paket-2',
		'description'	=> __('Menampilkan tarif paket baris kedua, maksimal hanya 3', 'babyhomecare'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '',
		'after_title'	=> ''
	));
	
	register_sidebar(array(
		'name'			=> __('Heading Promo', 'babyhomecare'),
		'id'			=> 'heading-promo',
		'description'	=> __('Menampilkan tagline promo', 'babyhomecare'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '',
		'after_title'	=> ''
	));
	
	register_sidebar(array(
		'name'			=> __('Footer Kontak', 'babyhomecare'),
		'id'			=> 'footer-kontak',
		'description'	=> __('Menampilkan text no. telp / hp kontak', 'babyhomecare'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '<h2 class="text-center">',
		'after_title'	=> '</h2>'
	));
}
add_action( 'widgets_init' , 'babyhomecare_widgets_init' );

?>