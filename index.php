<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!--
    <link href="style.css" rel="stylesheet" type="text/css">
    
    <link href="<?php echo get_option('home');?>/wp-content/themes/babycare/mystyle.css" rel="stylesheet" type="text/css">
    -->
  
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php
      if(is_singular())wp_enqueue_script('comment-reply');
    ?>
    <?php wp_head();?>
  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top hidden-md hidden-lg">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           

            <a class="mynavbar-brand" href="<?php echo esc_url( home_url( '/' ) );?>"><span>
            <?php
                /*Menampilkan logo atau site title*/
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                        echo '<img src="'. esc_url( $logo[0] ) .'">';
                } else {
                        echo get_bloginfo( 'name' );
                }
                ?>
            </span></a>

            
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
          <!--<ul class="nav navbar-nav navbar-right">
            <li>
              <a href="index.html#">Home</a>
            </li>
            <li>
              <a href="#prices">Paket</a>
            </li>
            <li>
              <a href="#kontak">Kontak</a>
            </li>
          </ul>-->
          <?php wp_nav_menu( 
              array(
                'theme_location'  => 'mobile-menu',
                'menu_class'    => 'nav navbar-nav navbar-right',
                'menu_id'     => 'mobile-menu-id'
              )
            );
            
            /*
            dari pembentukan <ul> atau tag <nav> class yg terlibat
            di masukkan ke menu_class
            */
          ?>
        </div>
      </div>
    </div>

    <div class="section hidden-sm hidden-xs" id="navbar-top">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="row">
              <div class="col-md-6">
                <?php
                /*Menampilkan logo atau site title*/
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                        echo '<img src="'. esc_url( $logo[0] ) .'">';
                } else {
                        echo '<h1 style="font-family:Lucida Calligraphy;color: #337cbb;margin:0px;padding-bottom:0px;font-size:38px;">'. get_bloginfo( 'name' ) .'</h1>';
                }
                ?>
                
                <?php $description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
				<p style="font-size: 18px;margin:0px;padding:0px;font-family: Gill Sans Extrabold, sans-serif;"><?php echo $description; ?></p>
				<?php endif; ?>

              </div>
              <div class="col-md-6">
                <?php wp_nav_menu( 
        						array(
        							'theme_location'	=> 'top-menu',
        							'menu_class'		=> 'nav nav-justified nav-pills pull-right',
        							'menu_id'			=> 'top-menu-id'
        						)
        					);
        					
        					/*
        					dari pembentukan <ul> atau tag <nav> class yg terlibat
        					di masukkan ke menu_class
        					*/
        				?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="landing">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="jumbotron text-center" id="jumbotron">
              <div class="row">
                <div class="col-md-6">
                  
                  <?php if( is_active_sidebar( 'jumbotron-image' ) ):?>
                    <?php dynamic_sidebar( 'jumbotron-image' );?>
                  <?php endif;?>
                
                </div>
                <div class="col-md-6">
				
                  <?php if( is_active_sidebar( 'jumbotron' ) ):?>
                    <?php dynamic_sidebar( 'jumbotron' );?>
                  <?php endif;?>
				  
                </div>
              </div>
              <br>
              <?php if( is_active_sidebar( 'jumbotron-link' ) ):?>
				<?php dynamic_sidebar( 'jumbotron-link' );?>
			  <?php endif;?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section mycss" id="layanan">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-justify">
            <div class="header-layanan">
              <h2 class="text-center">
                <i class="fa fa-fw fa-heart"></i>Layanan Kami</h2>
            </div>
            <div class="row">
              <?php
              if(have_posts()) :
              ?>
              <div class='col-md-6'>
                <ul class='media-list'>
				  <?php
					$loop = 1;
					while(have_posts()) : the_post();
				  ?>
				  <?php if($loop > 2): $loop=1;?>
						</ul>
					  </div>
					 <div class='col-md-6'>
					  <ul class='media-list'>
				  <?php endif;?>

                  <li class="media">
                    <a class="pull-left" href="#">
                      <!--<img class="media-object" src="images\baby-mandi.jpeg" height="100" width="100">-->
                      <?php the_post_thumbnail('thumbnail', array('class'=>'media-object'));?>
                    </a>
                    <div class="media-body">
                      <h4 class="media-heading text-center">
                        <strong><?php the_title();?></strong>
                      </h4>
                      <?php the_content();?>
                    </div>
                  </li>
				  <?php
					  $loop++;
					endwhile;
				  ?>
					</ul>
				  </div>
				  <?php endif;?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <section id="prices">
      <div class="container">
        <div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
				  <div class="col-lg-6 col-lg-offset-3 text-center">
					<h2>
					  <span class="ion-minus"></span>
					  Paket Perawatan Bayi
					  <span class="ion-minus"></span>
					</h2>
					<p>Tarif paket setiap kedatangan</p>
					<br>
				  </div>
				</div>
				<div class="prices-box">
					<div class="row">
						
						<?php if( is_active_sidebar( 'paket-1' ) ):?>
							<?php dynamic_sidebar( 'paket-1' );?>
						<?php endif;?>
						
					</div>
					<div class="row">
					
						<?php if( is_active_sidebar( 'paket-2' ) ):?>
							<?php dynamic_sidebar( 'paket-2' );?>
						<?php endif;?>
						
					</div>
				</div>
				
				<?php if( is_active_sidebar( 'heading-promo' ) ):?>
					<?php dynamic_sidebar( 'heading-promo' );?>
				<?php endif;?>
			</div>
        </div>
      </div>
    </section>
    
    <div class="section section-info" id="kontak">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            
			<?php if( is_active_sidebar( 'footer-kontak' ) ):?>
				<?php dynamic_sidebar( 'footer-kontak' );?>
			<?php endif;?>
			
          </div>
        </div>
      </div>
    </div>
    <footer class="mycss section section-primary" id="myfooter">
      <div class="container">
        <div class="row">
          <div class="col-md-offset-1 col-sm-10 text-center">© kaneisha.web.id | 2018</div>
        </div>
      </div>
    </footer>
  

</body></html>